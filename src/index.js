const loadBtn = document.getElementById("load");

async function addNamesToList(ul_id, num) {
  const ul = document.getElementById(ul_id);

  // Fetching random user data
  const response = await fetch(`https://randomuser.me/api/?results=${num}`);
  const data = await response.json();
  const names = data.results.map(
    (user) => `${user.name.first} ${user.name.last}`
  );

  // Appending each names to the ul element
  names.forEach((name) => {
    const li = document.createElement("li");
    li.innerText = name;
    ul.appendChild(li);
  });
}

(function () {
  addNamesToList("ul", 10);

  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 1.0,
  };

  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        addNamesToList("ul", 10);
      }
    });
  }, options);

  observer.observe(loadBtn);
})();

loadBtn.onclick = () => {
  addNamesToList("ul", 10);
};
