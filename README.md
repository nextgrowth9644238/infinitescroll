# InfiniteScroll

## How I implemented it

- I am using the "Random User Generator" API to fetch random user data and extract the names to add to the list.

- Used IntersectionObserver API, so when the target element enters or leaves our viewport then a callback will be fired.

- So I added an IntersectionObserver over the 'loading more names' button

- So Whenever the button enters our view port a callback will be fired which will add li tags under the existing ul tags.

- Deployment Link https://infinitescroll-1dhsgkfqp-e44rfrtt5.vercel.app/

- Video URL - https://www.loom.com/share/ad77d91d1105485796632f63b1beb073?sid=67d16058-ca2f-44d9-b31a-5f5c1f9c5431